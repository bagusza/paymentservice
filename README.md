
#  SIMS PPOB
The transaction service small project for my portofolio sample purpose.






## Features

- CRUD operations
- JWT authorization access
- Customizable error handling
- Allow user to register, login, and do transaction

## Stack
- Spring Boot
- JWT
- Postgres


## Swagger
![Springdoc](https://gcdnb.pbrd.co/images/ZPTQ5bbF1zxR.png?o=1){ width=70% }

## ER Diagram
![Diagram](https://gcdnb.pbrd.co/images/FbPgunq9uYxK.png?o=1){ width=70%}



## Live Demo

https://paymentservice-production.up.railway.app/

