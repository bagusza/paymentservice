package com.spring.tht.paymentservice.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;

import com.mgnt.utils.TextUtils;
import com.spring.tht.paymentservice.model.Attribute;
import com.spring.tht.paymentservice.model.request.RequestInfo;

import jakarta.servlet.http.HttpServletRequest;

public class CommonUtils {

    private CommonUtils(){}
    
    public static RequestInfo constructRequestInfo(String operation, Object requestPayload) {
        return new RequestInfo()
                .setOperationName(operation)
                .setRequestAt(Calendar.getInstance().getTime())
                .setCorrelationId(UUID.randomUUID().toString())
                .setRequestPayload(requestPayload);
    }

    public static RequestInfo constructRequestInfo(String operation, String correlationId, Object requestPayload, HttpServletRequest servletRequest) {
        if (servletRequest != null) {
            Map<String, String> headerMap = new HashMap<>();
            Enumeration<String> headerNames = servletRequest.getHeaderNames();

            while (headerNames.hasMoreElements()) {
                String key = headerNames.nextElement();
                String value = servletRequest.getHeader(key);
                headerMap.put(key, value);
            }
            return new RequestInfo()
                    .setOperationName(operation)
                    .setUri(servletRequest.getRequestURI())
                    .setMethod(servletRequest.getMethod())
                    .setHost(servletRequest.getRemoteHost())
                    .setRequestAt(Calendar.getInstance().getTime())
                    .setCorrelationId(StringUtils.defaultString(correlationId, UUID.randomUUID().toString()))
                    .setQueryParam(servletRequest.getQueryString())
                    .setAuthorization(StringUtils.defaultString(headerMap.get("authorization")))
                    .setRequestHeaders(headerMap)
                    .setRequestPayload(requestPayload)
                    .setHttpServletRequest(servletRequest);
        } else {
            return constructRequestInfo(operation, requestPayload);
        }
    }

    /**
     * fix cwe-89 vulnerability
     * 
     * @param query
     * @return
     */
    public static String cleanSql(String query) {
        return new PreparedStatementCreatorFactory(query).getSql();
    }

    public static String[] cleanStackTrace(Exception e) {
        try {
            return StringUtils.split(
                    StringUtils.replace(TextUtils.getStacktrace(e, true, "com.spring.tht.paymentservice"), "\t", ""),
                    "\n");
        } catch (Exception ex) {
            return new String[0];
        }
    }

    public static boolean isTypeMatch(String patternSet, String message){
        if(patternSet == null){
            return false;
        }
        Pattern pattern = Pattern.compile(patternSet);
        Matcher matcher = pattern.matcher(message);
        return matcher.find();
    }

    public static Map<String,Object> generateMap(Attribute... d){
        Map<String, Object> m = new HashMap<>();
        for(Attribute i : d){
            m.put(i.getName(), i.getValue());
        }
        return m;
    }

    public static String invoiceId(){
        String id = "INV";
        try {
            String parsed = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
            return id+parsed;
        }catch (Exception e){
            throw new IllegalArgumentException("invalid format", e);
        }
    }

    public static String strByDate(){
        try {
            return new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
        }catch (Exception e){
            throw new IllegalArgumentException("invalid format", e);
        }
    }

    public static String getCurrentEndpoint(HttpServletRequest request){
        String fullURL = request.getRequestURL().toString();
        return fullURL.substring(0,StringUtils.ordinalIndexOf(fullURL, "/", 3)); 
     }

     public static Date modifyDateLayout(String inputDate) {
        Date result;
        try{
            result = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(inputDate);
        }catch (ParseException p){
            result = null;
        }
        return result;
    }

    public static String reformatFullDate(Date date) {
        String parseDate = null;
        if(date!=null){
            try {
                parseDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
            }catch (Exception e){
                return null;
            }
        }
        return parseDate;
    }
}
