package com.spring.tht.paymentservice.model.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ServiceRs {
    @JsonProperty("service_code")
    @SerializedName("service_code")
    private String serviceCode;
    @JsonProperty("service_name")
    @SerializedName("service_name")
    private String serviceName;
    @JsonProperty("service_icon")
    @SerializedName("service_icon")
    private String serviceIcon;
    @JsonProperty("service_tariff")
    @SerializedName("service_tariff")
    private BigDecimal serviceTariff;
}
