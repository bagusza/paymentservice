package com.spring.tht.paymentservice.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class PaymentRq {
    @JsonProperty("service_code")
    @SerializedName("service_code")
    private String serviceCode;
}
