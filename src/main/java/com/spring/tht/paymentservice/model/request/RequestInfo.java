package com.spring.tht.paymentservice.model.request;

import java.util.Date;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RequestInfo {
    private String operationName;
    private String host;
    private String uri;
    private String method;
    private String correlationId;
    private Date requestAt;
    private Date requestEnd;
    private String completionStatus;
    private Object errorDetails;
    private String queryParam;
    private String authorization;
    private Object requestHeaders;
    private Object requestPayload;
    private Object responseHeaders;
    private Object responsePayload;
    private Object miscellaneous;
    private String statusCode;
    private String traceId;
    private HttpServletRequest httpServletRequest;
}
