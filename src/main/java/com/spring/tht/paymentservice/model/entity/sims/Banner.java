package com.spring.tht.paymentservice.model.entity.sims;

import lombok.Data;

@Data
public class Banner {
    private String bannerName;
    private String bannerImage;
    private String bannerDescription;
}
