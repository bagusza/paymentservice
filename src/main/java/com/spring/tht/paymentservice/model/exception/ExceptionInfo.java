package com.spring.tht.paymentservice.model.exception;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ExceptionInfo {
    @SerializedName("http_code")
    private int httpCode;
    private String status;
    private String code;
    private String message;
    @SerializedName("exception_code")
    private String exceptionCode;
    @SerializedName("exception_name")
    private String exceptionName;
    @SerializedName("contact_type")
    private String contactType;
    @SerializedName("contact_sid")
    private String serviceId;
    @SerializedName("contact_channel")
    private String contactChannel;
}