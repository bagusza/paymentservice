package com.spring.tht.paymentservice.model.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TransactionRs {
    @JsonProperty("invoice_number")
    @SerializedName("invoice_number")
    private String invoiceNumber;
    @JsonIgnore
    private BigDecimal memberId;
    @JsonProperty("service_code")
    @SerializedName("service_code")
    private String serviceCode;
    @JsonProperty("service_name")
    @SerializedName("service_name")
    private String serviceName;
    @JsonProperty("transaction_type")
    @SerializedName("transaction_type")
    private String transactionType;
    @JsonProperty("total_amount")
    @SerializedName("total_amount")
    private Long totalAmount;
    @JsonProperty("created_on")
    @SerializedName("created_on")
    private String createdOn;
}
