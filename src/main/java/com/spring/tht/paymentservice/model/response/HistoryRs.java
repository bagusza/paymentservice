package com.spring.tht.paymentservice.model.response;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class HistoryRs {
    private Integer offset;
    private Integer limit;
    private List<Records> records = new LinkedList<>();
    
    @Data
    @Accessors(chain = true)
    public static class Records{
        @JsonProperty("invoice_number")
        @SerializedName("invoice_number")
        private String invoiceNumber;
        @JsonProperty("transaction_type")
        @SerializedName("transaction_type")
        private String transactionType;
        @JsonProperty("description")
        @SerializedName("description")
        private String description;
        @JsonProperty("total_amount")
        @SerializedName("total_amount")
        private BigDecimal totalAmount;
        @JsonProperty("created_on")
        @SerializedName("created_on")
        private String createdOn;
    }
}


