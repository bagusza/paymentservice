package com.spring.tht.paymentservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Attribute {
    private String name;
    private String value;
}
