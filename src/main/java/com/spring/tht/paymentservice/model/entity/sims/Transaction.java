package com.spring.tht.paymentservice.model.entity.sims;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Transaction {
    private String invoiceNumber;
    @JsonIgnore
    private BigDecimal memberId;
    private String serviceCode;
    private String serviceName;
    private String transactionType;
    private Long totalAmount;
    private String createdOn;
    @JsonIgnore
    private String description;
}
