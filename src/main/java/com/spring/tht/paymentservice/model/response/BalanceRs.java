package com.spring.tht.paymentservice.model.response;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BalanceRs {
    private BigDecimal balance;
}
