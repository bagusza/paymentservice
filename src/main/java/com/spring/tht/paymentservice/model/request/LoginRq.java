package com.spring.tht.paymentservice.model.request;

import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginRq {
    @NotBlank(message = "Parameter email wajib diisi")
    @Email(message = "Parameter email tidak sesuai format", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;
    @NotBlank(message = "Parameter password wajib diisi")
    @Length(min = 8, message = "Parameter password minimal 8 karakter")
    private String password;
}
