package com.spring.tht.paymentservice.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import jakarta.validation.constraints.Min;
import lombok.Data;

@Data
public class TopupRq {
    @JsonProperty("top_up_amount")
    @SerializedName("top_up_amount")
    @Min(value = 1L, message = "The value must be positive")
    private Long topupAmount;
}
