package com.spring.tht.paymentservice.model.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.spring.tht.paymentservice.model.enums.CompletionStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ApiFault {
    private CompletionStatus status;
    @JsonIgnore
    private int httpCode = 0;
    private String type;
    private String message;
    private String detail;
    private String target;
    private Object trace;

    @JsonIgnore
    public String error() {
        return String.format("%s:%s:%s", type, message, detail);
    }

    @JsonIgnore
    public String description() {
        if (408 == httpCode) return String.format("%s | %s | %s", type, detail, target);
        return String.format("%s | %s", type, detail);
    }
}