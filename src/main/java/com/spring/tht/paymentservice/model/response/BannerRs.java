package com.spring.tht.paymentservice.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class BannerRs {
    @JsonProperty("banner_name")
    @SerializedName("banner_name")
    private String bannerName;
    @JsonProperty("banner_image")
    @SerializedName("banner_image")
    private String bannerImage;
    @JsonProperty("description")
    @SerializedName("description")
    private String bannerDescription;
}
