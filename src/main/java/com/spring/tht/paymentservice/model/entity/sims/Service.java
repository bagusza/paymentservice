package com.spring.tht.paymentservice.model.entity.sims;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

@Data
public class Service {
    private String serviceCode;
    private String serviceName;
    private String serviceIcon;
    private String serviceDescription;
    private Long serviceTariff;
    private Long serviceId;

    public String getServiceDescription(){
        return StringUtils.defaultString(this.serviceDescription);
    }
}
