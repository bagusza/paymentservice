package com.spring.tht.paymentservice.model.enums;

import org.springframework.http.HttpStatus;

public enum CompletionStatus {
    SUCCESS,
    BUSINESS_ERROR,
    SYSTEM_ERROR;

    public static CompletionStatus construct(HttpStatus status) {
        if (status.is2xxSuccessful()) {
            return CompletionStatus.SUCCESS;
        } else if (status.is5xxServerError() || HttpStatus.REQUEST_TIMEOUT.equals(status)) {
            return CompletionStatus.SYSTEM_ERROR;
        } else {
            return CompletionStatus.BUSINESS_ERROR;
        }
    }
}
