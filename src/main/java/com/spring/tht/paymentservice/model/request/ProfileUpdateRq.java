package com.spring.tht.paymentservice.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ProfileUpdateRq {
    @JsonProperty("first_name")
    @SerializedName("first_name")
    private String firstName;
    @JsonProperty("last_name")
    @SerializedName("last_name")
    private String lastName;
}
