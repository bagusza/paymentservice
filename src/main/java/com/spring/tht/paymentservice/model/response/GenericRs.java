package com.spring.tht.paymentservice.model.response;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.exception.definition.CommonException;
import com.spring.tht.paymentservice.model.enums.CompletionStatus;
import com.spring.tht.paymentservice.model.exception.ExceptionInfo;
import com.spring.tht.paymentservice.model.exception.ExceptionMap;
import com.spring.tht.paymentservice.utils.CommonUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
* generic response info
*/
@Getter
@Setter
@ToString
@Accessors(chain = true)
@Slf4j
@Configuration
public class GenericRs implements IBaseResponse {
    private int status;
    private String message;
    private Object data = null;

    @Override
    public void setSuccess() {
        this.status = 0;
        this.message = "Sukses";
    }

    @Override
    public void setSuccess(String message) {
        this.status = 0;
        this.message = message;
    }

    @Override
    public void setSuccess(Object data) {
        setSuccess();
        this.data = data;
    }

    @Override
    public void setData(Object data){
        this.data = data;
    }

    @Override
    public void overrideException(ExceptionInfo exception) {
        log.error("detect exception: {}{}{}", exception.getStatus(), exception.getCode(), exception.getMessage());
        if (exception.getStatus() != null) {
            this.status = exception.getStatus().equals(CompletionStatus.BUSINESS_ERROR.name()) ? 140 : 500;
        }
        this.message = StringUtils.defaultString(exception.getMessage());
    }

    @Override
    public void setCommonException(CommonException exception) {
        if (exception.getStatus() != null) {
            this.status = exception.getStatus().equals(CompletionStatus.BUSINESS_ERROR) ? 140 : 500;
        }
        this.message = StringUtils.defaultString(exception.getMessage(), exception.getDisplayMessage());
    }

    @Override
    public void overrideExceptionByMap(ResponseInfo response){
        for (ExceptionMap map : AppConstant.EXCEPTION_MAP) {
            if (CommonUtils.isTypeMatch(map.getPattern(), this.message.toLowerCase())) {
                this.status = map.getStatus();
                this.message = map.getMessage();
                response.setHttpStatus(HttpStatus.valueOf(NumberUtils.toInt(map.getHttpStatus())));
                break;
            }
        }
    }
}