package com.spring.tht.paymentservice.model.request;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class RegistrationRq {
    @NotBlank(message = "Parameter email wajib diisi")
    @Email(message = "Parameter email tidak sesuai format", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;
    @JsonProperty("first_name")
    @SerializedName("first_name")
    private String firstName;
    @JsonProperty("last_name")
    @SerializedName("last_name")
    private String lastName;
    @NotBlank(message = "Parameter password wajib diisi")
    @Length(min = 8, message = "Parameter password minimal 8 karakter")
    private String password;
}
