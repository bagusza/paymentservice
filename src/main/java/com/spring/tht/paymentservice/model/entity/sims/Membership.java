package com.spring.tht.paymentservice.model.entity.sims;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class Membership {
    @JsonIgnore
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    @JsonIgnore
    private String password;
    private String profileImage;
    @JsonIgnore
    private BigDecimal balance;
}
