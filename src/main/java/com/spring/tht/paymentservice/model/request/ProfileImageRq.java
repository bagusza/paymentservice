package com.spring.tht.paymentservice.model.request;

import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class ProfileImageRq {
    @NotEmpty(message = "file is mandatory")
    private MultipartFile file;
}
