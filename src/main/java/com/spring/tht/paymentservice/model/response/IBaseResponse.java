package com.spring.tht.paymentservice.model.response;

import com.spring.tht.paymentservice.exception.definition.CommonException;
import com.spring.tht.paymentservice.model.exception.ExceptionInfo;

public interface IBaseResponse {
    void setSuccess();
    void setSuccess(String message);
    void setSuccess(Object data);
    void setData(Object data);
    void overrideException(ExceptionInfo exception);
    void setCommonException(CommonException commonException);
    void overrideExceptionByMap(ResponseInfo response);
}