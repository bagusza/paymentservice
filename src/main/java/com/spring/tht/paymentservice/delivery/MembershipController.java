package com.spring.tht.paymentservice.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.request.LoginRq;
import com.spring.tht.paymentservice.model.request.ProfileUpdateRq;
import com.spring.tht.paymentservice.model.request.RegistrationRq;
import com.spring.tht.paymentservice.model.request.RequestInfo;
import com.spring.tht.paymentservice.model.response.ResponseInfo;
import com.spring.tht.paymentservice.service.JwtService;
import com.spring.tht.paymentservice.usecase.MembershipUsecase;
import com.spring.tht.paymentservice.utils.CommonUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Validated
@Tag(name = "1. Module Membership")
public class MembershipController {

    @Autowired
    MembershipUsecase membershipUsecase;
    @Autowired
    JwtService jwtService;
    
    @PostMapping("/registration")
    @Operation(summary = "Register new membership", description = "Returns a result been registered")
    public ResponseEntity<Object> postRegistration(
        @Parameter(description = "Request body payload") @RequestBody(required = true) @Valid RegistrationRq bodyRq,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("membership-registration", null, bodyRq, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = membershipUsecase.register(request, bodyRq);
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    @PostMapping("/login")
    @Operation(summary = "Login user", description = "Return a jwt token")
    public ResponseEntity<Object> postLogin(
        @Parameter(description = "Request body payload") @RequestBody(required = true) @Valid LoginRq bodyRq,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("membership-login", null, bodyRq, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = membershipUsecase.login(request, bodyRq);
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }
    
    @GetMapping("/profile")
    @Operation(summary = "Profile membership", description = "Return profile details", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> getProfile(
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("membership-profile", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = membershipUsecase.profile(request, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    @PutMapping("/profile/update")
    @Operation(summary = "Profile update", description = "Return a updated data", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> postProfileUpdate(
        @Parameter(description = "Request body payload") @RequestBody(required = true) @Valid ProfileUpdateRq bodyRq,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("membership-profile-update", null, bodyRq, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        request.getAuthorization();
        ResponseInfo response = membershipUsecase.profileUpdate(request, bodyRq, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    @PutMapping(value = "/profile/image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Profile update", description = "Return a updated image uri", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> postProfileUpdateImage(
        @RequestParam(required = false) MultipartFile file,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("membership-profile-image", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = membershipUsecase.profileImage(request, file, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }
}
