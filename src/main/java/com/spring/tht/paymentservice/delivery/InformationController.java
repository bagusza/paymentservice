package com.spring.tht.paymentservice.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.request.RequestInfo;
import com.spring.tht.paymentservice.model.response.BannerRs;
import com.spring.tht.paymentservice.model.response.ResponseInfo;
import com.spring.tht.paymentservice.model.response.ServiceRs;
import com.spring.tht.paymentservice.repository.SimsRepository;
import com.spring.tht.paymentservice.service.JwtService;
import com.spring.tht.paymentservice.utils.CommonUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Validated
@Tag(name = "2. Module Information")
public class InformationController {

    @Autowired
    SimsRepository simsRepository;
    @Autowired
    JwtService jwtService;

    /* 
     * point 1: Buat data list banner sesuai dokumentasi Response dibawah, usahakan banner ini tidak di hardcode, melainkan ambil dari database.
     * point 2: Tidak perlu membuatkan module CRUD banner.
     * point 3: Handling Response sesuai dokumentasi Response dibawah.
     ! tidak memerlukan akses token
     */
    @GetMapping("/banner")
    @Operation(summary = "Banner information", description = "Returns banner list")
    public ResponseEntity<Object> getBanner(HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("information-banner", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = new ResponseInfo();
        response.setSuccess();
        response.setData(simsRepository.getAllBanner(BannerRs.class));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    /* 
     * point 1: Buat data list banner sesuai dokumentasi Response dibawah, usahakan banner ini tidak di hardcode, melainkan ambil dari database.
     * point 2: Tidak perlu membuatkan module CRUD service.
     * point 3: Handling Response sesuai dokumentasi Response dibawah.
     ! menggunakan akses token
     */
    @GetMapping("/services")
    @Operation(summary = "Services information", description = "Returns service list", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> getService(
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("information-service", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = new ResponseInfo();
        try {
            jwtService.validTokenThrowable(request.getAuthorization());
            response.setSuccess();
            response.setData(simsRepository.getAllServices(ServiceRs.class));
        } catch (Exception e) {
            response.setException(e);
        }
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }
}
