package com.spring.tht.paymentservice.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.request.PaymentRq;
import com.spring.tht.paymentservice.model.request.RequestInfo;
import com.spring.tht.paymentservice.model.request.TopupRq;
import com.spring.tht.paymentservice.model.response.ResponseInfo;
import com.spring.tht.paymentservice.service.JwtService;
import com.spring.tht.paymentservice.usecase.TransactionUsecase;
import com.spring.tht.paymentservice.utils.CommonUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Validated
@Tag(name = "3. Module Transaction")
public class TransactionController {

    @Autowired
    TransactionUsecase transactionUsecase;
    @Autowired
    JwtService jwtService;

    /* 
     * point 1: Service ini harus menggunakan Bearer Token JWT untuk mengaksesnya.
     * point 2: Tidak ada parameter email di query param url ataupun request body, parameter email diambil dari payload JWT yang didapatkan dari hasil login.
     * point 3: Handling Response sesuai dokumentasi Response dibawah.
     ! memerlukan akses token
     */

    @GetMapping("/balance")
    @Operation(summary = "Member balance", description = "Return member balance", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> getBalance(
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("transaction-balance", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = transactionUsecase.balance(request, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    /* 
     * point 1: Service ini harus menggunakan Bearer Token JWT untuk mengaksesnya.
     * point 2: Tidak ada parameter email di query param url ataupun request body, parameter email diambil dari payload JWT yang didapatkan dari hasil login.
     * point 3: Setiap kali melakukan Top Up maka balance / saldo dari User otomatis bertambah
     * point 4: Parameter amount hanya boleh angka saja dan tidak boleh lebih kecil dari 0
     * point 5: Pada saat Top Up set transaction_type di database menjadi TOPUP
     * point 6: Handling Response sesuai dokumentasi Response dibawah.
     ! memerlukan akses token
     */
    @PostMapping("/topup")
    @Operation(summary = "Topup balance", description = "Return result topup balance status", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> postTopup(
        @Parameter(description = "Request body payload") @RequestBody(required = false) @Valid TopupRq bodyRq,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("transaction-topup", null, bodyRq, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = transactionUsecase.topup(request, bodyRq, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    /* 
     * point 1: Service ini harus menggunakan Bearer Token JWT untuk mengaksesnya.
     * point 2: Tidak ada parameter email di query param url ataupun request body, parameter email diambil dari payload JWT yang didapatkan dari hasil login.
     * point 3: Setiap kali melakukan Transaksi harus dipastikan balance / saldo mencukupi.
     * point 4: Pada saat Transaction set transaction_type di database menjadi PAYMENT.
     * point 5: andling Response sesuai dokumentasi Response dibawah.
     * point 6: Response invoice_number untuk formatnya generate bebas.
     ! memerlukan akses token
     */
    @PostMapping("/transaction")
    @Operation(summary = "Create Transaction", description = "Return result service transaction", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> postTransaction(
        @Parameter(description = "Request body payload") @RequestBody(required = false) @Valid PaymentRq bodyRq,
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("transaction-create", null, bodyRq, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = transactionUsecase.transaction(request, bodyRq, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }

    /* 
     * point 1: Service ini harus menggunakan Bearer Token JWT untuk mengaksesnya.
     * point 2: Tidak ada parameter email di query param url ataupun request body, parameter email diambil dari payload JWT yang didapatkan dari hasil login.
     * point 3: erdapat parameter limit yang bersifat opsional, jika limit tidak dikirim maka tampilkan semua data.
     * point 4: Data di order dari yang paling baru berdasarkan transaction date (created_on).
     * point 5: Handling Response sesuai dokumentasi Response dibawah.
     ! memerlukan akses token
     */

    @GetMapping("/transaction/history")
    @Operation(summary = "Transaction history", description = "Return user transaction history", security = @SecurityRequirement(name = "bearerAuth"))
    public ResponseEntity<Object> getTransactionHistory(
        @RequestParam(required = false, defaultValue = "0") Integer offset,
        @RequestParam(required = false, defaultValue = "3") Integer limit, 
        HttpServletRequest servletRequest){
        RequestInfo request = CommonUtils.constructRequestInfo("transaction-history", null, null, servletRequest);
        log.info(AppConstant.LOG_REQUEST_RECEIVED, request.getCorrelationId(), request.getOperationName(), servletRequest.getQueryString());
        ResponseInfo response = transactionUsecase.history(request, offset, limit, jwtService.validateAndGetUser(request.getAuthorization()));
        log.info(AppConstant.LOG_REQUEST_END, request.getCorrelationId(), request.getOperationName(), response);
        return new ResponseEntity<>(response.getBody(), response.getHttpHeaders(), response.getHttpStatus());
    }
}
