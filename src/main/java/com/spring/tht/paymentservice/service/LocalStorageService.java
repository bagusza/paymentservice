package com.spring.tht.paymentservice.service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.spring.tht.paymentservice.config.variable.AppConstant;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LocalStorageService {

    private Path rootPath;

    @PostConstruct
    public void init() {
        try {
            String separator = FileSystems.getDefault().getSeparator();
            Path source = Paths.get(this.getClass().getResource(separator).getPath());
            rootPath = Paths.get(source.toAbsolutePath() + separator + "resources" + separator);
            Files.createDirectories(rootPath);
        } catch (IOException e) {
            log.error(AppConstant.LOG_DEFAULT_ERROR, "Could not initialize resources folder", e.getLocalizedMessage(), e);
        } catch (RuntimeException re){
            throw re;
        }
    }

    public String getResourcesPath(){
        return rootPath.toString();
    }

    public void save(MultipartFile file) {
        try {
            Files.copy(file.getInputStream(), this.rootPath.resolve(file.getOriginalFilename()));
        } catch (IOException e ) {
            log.error(AppConstant.LOG_DEFAULT_ERROR, "Multipart save file", e.getLocalizedMessage(), e);
        } catch (RuntimeException re){
            throw re;
        }
    }

    public boolean validateImageFormat(MultipartFile file){
        return ((file.getContentType().equals("image/jpeg")) || (file.getContentType().equals("image/png")));
    }

    public boolean saveImage(MultipartFile file){
        if(validateImageFormat(file)){
            save(file);
            return true;
        }
        return false;
    }
}
