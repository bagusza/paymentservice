package com.spring.tht.paymentservice.service;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.spring.tht.paymentservice.config.properties.AppProperties;
import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.request.LoginRq;

@Service
public class JwtService {

    @Autowired
    AppProperties appProperties;

    public String createToken(String email, String password) {
        Algorithm algorithm = Algorithm.HMAC256(appProperties.getJWT_SECRET());
        return JWT.create()
                .withIssuer(AppConstant.APP_NAME)
                .withSubject("sims")
                .withClaim("email", email)
                .withClaim("password", password)
                .withIssuedAt(new Date())
                .withExpiresAt(Instant.now().plusSeconds(appProperties.getJWT_EXPIRATION()))
                .withJWTId(UUID.randomUUID().toString())
                // .withNotBefore(new Date(System.currentTimeMillis() + 1000L))
                .sign(algorithm);
    }

    public boolean isValidToken(String jwt) {
        try {
            if(StringUtils.isAnyEmpty(jwt))
                throw new JWTDecodeException("invalid jwt token");

            String[] str = jwt.split(" ");
            Algorithm algorithm = Algorithm.HMAC256(appProperties.getJWT_SECRET());
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(AppConstant.APP_NAME)
                    .build();
            DecodedJWT decodedJWT = verifier.verify(str[1]);
            decodedJWT.getAlgorithm();
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }

    public void validTokenThrowable(String jwt) throws JWTDecodeException {
        try {
            if(StringUtils.isAnyEmpty(jwt))
                throw new JWTDecodeException("invalid jwt token");

            String[] str = jwt.split(" ");
            Algorithm algorithm = Algorithm.HMAC256(appProperties.getJWT_SECRET());
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(AppConstant.APP_NAME)
                    .build();
            DecodedJWT decodedJWT = verifier.verify(str[1]);
            decodedJWT.getAlgorithm();
        } catch (JWTDecodeException e) {
            throw new JWTDecodeException("invalid jwt token", e);
        }
    }

    public LoginRq getUserCreds(String jwt) {
        if(StringUtils.isAnyEmpty(jwt))
            throw new JWTDecodeException("invalid jwt token");

        String[] str = jwt.split(" ");
        Algorithm algorithm = Algorithm.HMAC256(appProperties.getJWT_SECRET());
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(AppConstant.APP_NAME)
                .build();
        DecodedJWT decodedJWT = verifier.verify(str[1]);
        return new LoginRq()
                .setEmail(decodedJWT.getClaim("email").asString())
                .setPassword(decodedJWT.getClaim("password").asString());
    }

    public LoginRq validateAndGetUser(String jwt) throws JWTDecodeException {
        validTokenThrowable(jwt);
        return getUserCreds(jwt);
    }
}