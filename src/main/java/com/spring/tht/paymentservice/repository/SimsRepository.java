package com.spring.tht.paymentservice.repository;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.tht.paymentservice.config.properties.AppProperties;
import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.entity.sims.Banner;
import com.spring.tht.paymentservice.model.entity.sims.Membership;
import com.spring.tht.paymentservice.model.entity.sims.Service;
import com.spring.tht.paymentservice.model.entity.sims.Transaction;
import com.spring.tht.paymentservice.utils.CommonUtils;

import jakarta.annotation.PostConstruct;

@Repository
@Transactional
public class SimsRepository {
    @Autowired
    @Qualifier(AppConstant.BEAN_JDBC_SIMS_POSTGRES)
    private JdbcTemplate jdbcTemplate;
    @Autowired
    @Qualifier(AppConstant.BEAN_APP_CONF)
    private AppProperties appProperties;
    private NamedParameterJdbcTemplate template;

    private String queryGetMembership;
    private String queryGetBanner;
    private String queryGetService;
    private String queryGetTransaction;

    private String queryInsertMembership;
    private String queryInsertBanner;
    private String queryInsertService;
    private String queryInsertTransaction;

    private String queryUpdateMembership;
    private String queryUpdateBanner;
    private String queryUpdateService;
    private String queryUpdateTransaction;

    @PostConstruct
    private void init(){
        template = new NamedParameterJdbcTemplate(jdbcTemplate);
        queryGetMembership = appProperties.getQUERY_SIMS().get("GET_MEMBERSHIP");
        queryGetBanner = appProperties.getQUERY_SIMS().get("GET_BANNER");
        queryGetService = appProperties.getQUERY_SIMS().get("GET_SERVICE");
        queryGetTransaction = appProperties.getQUERY_SIMS().get("GET_TRANSACTION");
        queryInsertMembership = appProperties.getQUERY_SIMS().get("INSERT_MEMBERSHIP");
        queryInsertBanner = appProperties.getQUERY_SIMS().get("INSERT_BANNER");
        queryInsertService = appProperties.getQUERY_SIMS().get("INSERT_SERVICE");
        queryInsertTransaction = appProperties.getQUERY_SIMS().get("INSERT_TRANSACTION");
        queryUpdateMembership = appProperties.getQUERY_SIMS().get("UPDATE_MEMBERSHIP");
        queryUpdateBanner = appProperties.getQUERY_SIMS().get("UPDATE_BANNER");
        queryUpdateService = appProperties.getQUERY_SIMS().get("UPDATE_SERVICE");
        queryUpdateTransaction = appProperties.getQUERY_SIMS().get("UPDATE_TRANSACTION");
    }

    public List<Membership> getMembers(String email, String password){
        String q = queryGetMembership + " WHERE email = :email AND password = :password";
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("email", StringUtils.defaultString(email));
        m.addValue("password", StringUtils.defaultString(password));
        return template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Membership.class));
    }

    public Membership getMember(String email, String password){
        List<Membership> members = getMembers(email, password);
        return members.isEmpty() ? null : members.get(0);
    }

    public boolean isValidMember(String email){
        String q = queryGetMembership + " WHERE email = :email";
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("email", StringUtils.defaultString(email));
        List<Membership> members = template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Membership.class));
        return !members.isEmpty();
    }

    public List<Banner> getBanners(String bannerName){
        String q = queryGetBanner + " WHERE banner_name= :bannername";
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("bannername", StringUtils.defaultString(bannerName));
        return template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Banner.class));
    }
    
    public Banner getBanner(String bannerName){
        List<Banner> banners = getBanners(bannerName);
        return banners.isEmpty() ? null : banners.get(0);
    }

    public List<Banner> getAllBanner(){
        return template.query(CommonUtils.cleanSql(queryGetBanner), new BeanPropertyRowMapper<>(Banner.class));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Object> getAllBanner(Class clazz){
        return template.query(CommonUtils.cleanSql(queryGetBanner), new BeanPropertyRowMapper<>(clazz));
    }

    public List<Service> getServices(String serviceCode){
        String q = queryGetService + " WHERE service_code= :servicecode";
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("servicecode", StringUtils.defaultString(serviceCode));
        return template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Service.class));
    }
    
    public Service getService(String serviceCode){
        List<Service> service = getServices(serviceCode);
        return service.isEmpty() ? null : service.get(0);
    }

    public List<Service> getAllServices(){
        return template.query(CommonUtils.cleanSql(queryGetService), new BeanPropertyRowMapper<>(Service.class));
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Object> getAllServices(Class clazz){
        return template.query(CommonUtils.cleanSql(queryGetService), new BeanPropertyRowMapper<>(clazz));
    }

    public List<Transaction> getTransactions(String invoiceNumber){
        String q = queryGetTransaction + " WHERE invoice_number= :invoicenumber";
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("invoicenumber", StringUtils.defaultString(invoiceNumber));
        return template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Transaction.class));
    }
    
    public Transaction getTransaction(String bannerName){
        List<Transaction> transactions = getTransactions(bannerName);
        return transactions.isEmpty() ? null : transactions.get(0);
    }

    public List<Transaction> getTransactions(long memberId, Integer offset, Integer limit){
        String q = queryGetTransaction + " WHERE member_id=:memberid ORDER BY created_on DESC {limit} {offset}"
            .replace("{limit}", StringUtils.defaultString("LIMIT " + limit.toString(), ""))
            .replace("{offset}", StringUtils.defaultString("OFFSET " + offset.toString(), ""));
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("memberid", memberId);
        return template.query(CommonUtils.cleanSql(q), m, new BeanPropertyRowMapper<>(Transaction.class));
    }

    public void insertMember(Membership... membership) throws DataAccessException{
        for (Membership m : membership) {
            template.update(CommonUtils.cleanSql(queryInsertMembership), mappingSourceMember(m));
        }
    }

    public void insertBanner(Banner... banner) throws DataAccessException{
        for (Banner b : banner) {
            template.update(CommonUtils.cleanSql(queryInsertBanner), mappingSourceBanner(b));
        }
    }

    public void insertService(Service... service) throws DataAccessException{
        for (Service s : service) {
            template.update(CommonUtils.cleanSql(queryInsertService), mappingSourceService(s));
        }
    }

    public void insertTransaction(Transaction... transaction) throws DataAccessException{
        for(Transaction t : transaction){
            template.update(CommonUtils.cleanSql(queryInsertTransaction), mappingSourceTransaction(t));
        }
    }

    public void updateMembership(Membership... membership) throws DataAccessException {
        for(Membership m : membership){
            template.update(CommonUtils.cleanSql(queryUpdateMembership), mappingSourceMember(m));
        }
    }

    public void updateBanner(Banner... banner) throws DataAccessException {
        for(Banner b : banner){
            template.update(CommonUtils.cleanSql(queryUpdateBanner), mappingSourceBanner(b));
        }
    }

    public void updateService(Service... service) throws DataAccessException {
        for(Service s : service){
            template.update(CommonUtils.cleanSql(queryUpdateService), mappingSourceService(s));
        }
    }

    public void updateTransaction(Transaction... transaction) throws DataAccessException {
        for(Transaction t : transaction){
            template.update(CommonUtils.cleanSql(queryUpdateTransaction), mappingSourceTransaction(t));
        }
    }

    private MapSqlParameterSource mappingSourceMember(Membership member){
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("id", member.getId());
        m.addValue("firstname", member.getFirstName());
        m.addValue("lastname", member.getLastName());
        m.addValue("email", member.getEmail());
        m.addValue("password", member.getPassword());
        m.addValue("profileimage", member.getProfileImage());
        m.addValue("balance", member.getBalance());
        return m;
    }

    private MapSqlParameterSource mappingSourceBanner(Banner banner){
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("bannername", banner.getBannerName());
        m.addValue("bannerimage", banner.getBannerImage());
        m.addValue("bannerdescription", banner.getBannerDescription());
        return m;
    }

    private MapSqlParameterSource mappingSourceService(Service service){
        MapSqlParameterSource m = new MapSqlParameterSource();
        m.addValue("servicecode", service.getServiceCode());
        m.addValue("servicename", service.getServiceName());
        m.addValue("serviceicon", service.getServiceIcon());
        m.addValue("servicedescription", service.getServiceDescription());
        m.addValue("servicetariff", service.getServiceTariff());
        m.addValue("serviceid", service.getServiceId());
        return m;
    }

    private MapSqlParameterSource mappingSourceTransaction(Transaction transaction){
        MapSqlParameterSource m =new MapSqlParameterSource();
        m.addValue("invoicenumber", transaction.getInvoiceNumber());
        m.addValue("memberid", transaction.getMemberId());
        m.addValue("servicecode", transaction.getServiceCode());
        m.addValue("servicename", transaction.getServiceName());
        m.addValue("transactiontype", transaction.getTransactionType());
        m.addValue("totalamount", transaction.getTotalAmount());
        m.addValue("createdon", Timestamp.valueOf(transaction.getCreatedOn()));
        return m;
    }
}
