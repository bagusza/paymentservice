package com.spring.tht.paymentservice.exception.handler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.spring.tht.paymentservice.config.properties.AppProperties;
import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.exception.definition.CommonException;
import com.spring.tht.paymentservice.model.response.ResponseInfo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    @Qualifier(AppConstant.BEAN_APP_CONF)
    private AppProperties appProperties;

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleNoHandlerFoundException {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getRequestURL() + ":" + ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleMethodArgumentNotValid {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());

        List<String> err = new ArrayList<>();
        for (ObjectError error: ex.getBindingResult().getAllErrors()) {
            err.add(error.getDefaultMessage());
        }
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, StringUtils.join(err, ", "));
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleHttpRequestMethodNotSupported {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleMissingPathVariable {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getVariableName() + ":" + ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleHttpMediaTypeNotSupported {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getContentType() + ":" + ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleHttpMediaTypeNotAcceptable {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleMissingServletRequestParameter {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getParameterName() + ":" + ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleServletRequestBindingException {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleConversionNotSupported(ConversionNotSupportedException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleConversionNotSupported {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getErrorCode() + ":" + ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleTypeMismatch {}: {}: {}", request.getHeader("requestid"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex.getMessage());
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleHttpMessageNotReadable {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, "required request body is missing");
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotWritable(HttpMessageNotWritableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleHttpMessageNotWritable {}", request.getHeader("request-id"));
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(MissingServletRequestPartException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleMissingServletRequestPart {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleBindException {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @ExceptionHandler(value = { RuntimeException.class })
    protected ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        log.warn("handleBindException {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.INTERNAL_SERVER_ERROR, request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn("handleAsyncRequestTimeoutException {}: {}: {}", request.getHeader("request-id"), request, ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.warn(ex.getMessage());
        ResponseInfo responseInfo = generateException(HttpStatus.valueOf(status.value()), request, ex);
        return new ResponseEntity<>(responseInfo.getBody(), responseInfo.getHttpStatus());
    }

    private ResponseInfo generateException(HttpStatus status, WebRequest request, Exception ex) {
        return generateException(HttpStatus.valueOf(status.value()), request, ex.getMessage());
    }

    public ResponseInfo generateException(HttpStatus status, WebRequest request, String ex) {
        ResponseInfo responseInfo = new ResponseInfo().setHttpStatus(status);
        responseInfo.setCommonException(new CommonException(status, request.getDescription(false), StringUtils.defaultString(ex), StringUtils.defaultString(ex)));
        log.warn("error: " + responseInfo.getError());
        return responseInfo;
    }
}