package com.spring.tht.paymentservice.exception.definition;

import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriComponents;

import com.spring.tht.paymentservice.model.enums.CompletionStatus;
import com.spring.tht.paymentservice.model.exception.ApiFault;
import com.spring.tht.paymentservice.utils.CommonUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonException extends Exception {
    private CompletionStatus status;
    private HttpStatus httpStatus;
    private String type;
    private String displayMessage;
    private String target;
    private Exception originException;

    public CommonException(HttpStatus status, String type, String displayMessage, String message, UriComponents target) {
        super(message);
        this.httpStatus = status;
        this.status = CompletionStatus.construct(status);
        this.type = type;
        this.displayMessage = displayMessage;
        if (target != null) {
            this.target = target.toString();
        }
    }

    public CommonException(HttpStatus status, String type, String displayMessage, String message) {
        super(message);
        this.httpStatus = status;
        this.status = CompletionStatus.construct(status);
        this.type = type;
        this.displayMessage = displayMessage;
    }

    public CommonException(HttpStatus status, String message){
        super(message);
        this.httpStatus = status;
        this.status = CompletionStatus.construct(status);
        this.displayMessage = message;
    }

    public CommonException(Exception e) {
        super(e.getMessage());
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        this.status = CompletionStatus.SYSTEM_ERROR;
        this.type = e.getClass().getSimpleName();
        this.displayMessage = "Unknown Error: " + e.getClass().getSimpleName();
        this.originException = e;
    }

    public ApiFault getApiFault() {
        return new ApiFault()
                .setHttpCode(httpStatus.value())
                .setStatus(status)
                .setType(type)
                .setMessage(displayMessage)
                .setDetail(type + ":" + super.getMessage())
                .setTarget(target)
                .setTrace(CommonUtils.cleanStackTrace((originException != null)?originException:this));
    }
}