package com.spring.tht.paymentservice.usecase;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.exception.definition.CommonException;
import com.spring.tht.paymentservice.model.entity.sims.Membership;
import com.spring.tht.paymentservice.model.entity.sims.Service;
import com.spring.tht.paymentservice.model.entity.sims.Transaction;
import com.spring.tht.paymentservice.model.request.LoginRq;
import com.spring.tht.paymentservice.model.request.PaymentRq;
import com.spring.tht.paymentservice.model.request.RequestInfo;
import com.spring.tht.paymentservice.model.request.TopupRq;
import com.spring.tht.paymentservice.model.response.BalanceRs;
import com.spring.tht.paymentservice.model.response.HistoryRs;
import com.spring.tht.paymentservice.model.response.HistoryRs.Records;
import com.spring.tht.paymentservice.model.response.ResponseInfo;
import com.spring.tht.paymentservice.model.response.TransactionRs;
import com.spring.tht.paymentservice.utils.CommonUtils;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TransactionUsecase extends BaseUsecase {

    Map<String, Service> cachedServiceMap = new HashMap<>();

    @PostConstruct
    void init(){
        // cache all service
        List<Service> services = simsRepository.getAllServices();
        for (Service s : services) {
            cachedServiceMap.put(s.getServiceCode(), s);
        }
    }

    public ResponseInfo balance(RequestInfo request, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            response.setSuccess("Get Balance Berhasil");
            response.setData(new BalanceRs().setBalance(member.getBalance()));
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo topup(RequestInfo request, TopupRq topupRq, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            Long calc = member.getBalance().longValue() + topupRq.getTopupAmount();
            member.setBalance(new BigDecimal(calc));
            simsRepository.updateMembership(member);

            Service service = simsRepository.getService("BALANCE_ADD");
            Transaction transaction = new Transaction()
                .setInvoiceNumber(CommonUtils.invoiceId())
                .setMemberId(new BigDecimal(member.getId()))
                .setServiceCode(service.getServiceCode())
                .setServiceName(service.getServiceName())
                .setDescription(service.getServiceDescription())
                .setTransactionType(AppConstant.TRANSACTION_TYPE.TOPUP.name())
                .setTotalAmount(topupRq.getTopupAmount())
                .setCreatedOn(CommonUtils.reformatFullDate(Calendar.getInstance().getTime()));
            simsRepository.insertTransaction(transaction);

            response.setSuccess("Top Up Balance berhasil");
            response.setData(new BalanceRs().setBalance(member.getBalance()));
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo transaction(RequestInfo request, PaymentRq bodyRq, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            Service service = simsRepository.getService(bodyRq.getServiceCode());
            if(ObjectUtils.anyNull(service))
                throw new CommonException(HttpStatus.BAD_REQUEST, "Service atau Layanan tidak ditemukan");

            if(service.getServiceTariff().longValue() > member.getBalance().longValue())
                throw new CommonException(HttpStatus.BAD_REQUEST, "Balance anda tidak cukup");

            Long calc = member.getBalance().longValue() - service.getServiceTariff().longValue();
            member.setBalance(new BigDecimal(calc));
            simsRepository.updateMembership(member);

            Transaction transaction = new Transaction()
                .setInvoiceNumber(CommonUtils.invoiceId())
                .setMemberId(new BigDecimal(member.getId()))
                .setServiceCode(service.getServiceCode())
                .setServiceName(service.getServiceName())
                .setTransactionType(AppConstant.TRANSACTION_TYPE.PAYMENT.name())
                .setTotalAmount(service.getServiceTariff())
                .setCreatedOn(CommonUtils.reformatFullDate(Calendar.getInstance().getTime()));
            simsRepository.insertTransaction(transaction);

            TransactionRs transactionRs = new TransactionRs()
                .setInvoiceNumber(transaction.getInvoiceNumber())
                .setMemberId(transaction.getMemberId())
                .setServiceCode(transaction.getServiceCode())
                .setServiceName(transaction.getServiceName())
                .setTransactionType(transaction.getTransactionType())
                .setTotalAmount(transaction.getTotalAmount())
                .setCreatedOn(transaction.getCreatedOn());

            response.setSuccess("Transaksi berhasil");
            response.setData(transactionRs);
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo history(RequestInfo request, Integer offset, Integer limit, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            HistoryRs historyRs = new HistoryRs()
                .setOffset(offset)
                .setLimit(limit);
            
            List<Transaction> transactions = simsRepository.getTransactions(member.getId(), offset, limit);
            for (Transaction t : transactions) {
                Records records = new Records()
                    .setInvoiceNumber(t.getInvoiceNumber())
                    .setTransactionType(t.getTransactionType())
                    .setDescription(ObjectUtils.toString(cachedServiceMap.get(t.getServiceCode()).getServiceDescription(), ()-> ""))
                    .setTotalAmount(new BigDecimal(t.getTotalAmount()))
                    .setCreatedOn(t.getCreatedOn());
                historyRs.getRecords().add(records);
            }
        
            response.setSuccess("Get History Berhasil");
            response.setData(historyRs);
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }
}
