package com.spring.tht.paymentservice.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spring.tht.paymentservice.config.properties.AppProperties;
import com.spring.tht.paymentservice.repository.SimsRepository;
import com.spring.tht.paymentservice.service.JwtService;
import com.spring.tht.paymentservice.service.LocalStorageService;

@Component
public class BaseUsecase {
    @Autowired
    AppProperties appProperties;
    @Autowired
    JwtService jwtService;
    @Autowired
    LocalStorageService localStorageService;
    @Autowired
    SimsRepository simsRepository;
}
