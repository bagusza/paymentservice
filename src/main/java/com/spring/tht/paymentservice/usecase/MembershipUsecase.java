package com.spring.tht.paymentservice.usecase;

import java.math.BigDecimal;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.exception.definition.CommonException;
import com.spring.tht.paymentservice.model.Attribute;
import com.spring.tht.paymentservice.model.entity.sims.Membership;
import com.spring.tht.paymentservice.model.request.LoginRq;
import com.spring.tht.paymentservice.model.request.ProfileUpdateRq;
import com.spring.tht.paymentservice.model.request.RegistrationRq;
import com.spring.tht.paymentservice.model.request.RequestInfo;
import com.spring.tht.paymentservice.model.response.ResponseInfo;
import com.spring.tht.paymentservice.utils.CommonUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MembershipUsecase extends BaseUsecase {
    
    public ResponseInfo register(RequestInfo request, RegistrationRq bodyRq){
        ResponseInfo response = new ResponseInfo();
        try {
            if(simsRepository.isValidMember(bodyRq.getEmail()))
                throw new CommonException(HttpStatus.NOT_ACCEPTABLE, String.format("Member %s sudah terdaftar", bodyRq.getEmail()));

            Membership member = new Membership();
            member.setEmail(bodyRq.getEmail());
            member.setPassword(bodyRq.getPassword());
            member.setFirstName(bodyRq.getFirstName());
            member.setLastName(bodyRq.getLastName());
            member.setBalance(new BigDecimal(0));
            simsRepository.insertMember(member);
            response.setSuccess("Registrasi berhasil silahkan login");
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo login(RequestInfo request, LoginRq bodyRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(bodyRq.getEmail(), bodyRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            if(!member.getPassword().equals(bodyRq.getPassword()))
                throw new CommonException(HttpStatus.BAD_REQUEST, "Username atau Password salah");

            response.setSuccess("Login Sukses");
            response.setData(CommonUtils.generateMap(new Attribute("token", jwtService.createToken(member.getEmail(), member.getPassword()))));
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo profile(RequestInfo request, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            if(!member.getPassword().equals(loginRq.getPassword()))
                throw new CommonException(HttpStatus.BAD_REQUEST, "Username atau Password salah");

            response.setSuccess();
            response.setData(member);
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo profileUpdate(RequestInfo request, ProfileUpdateRq profileRq, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            member.setFirstName(profileRq.getFirstName());
            member.setLastName(profileRq.getLastName());
            simsRepository.updateMembership(member);

            response.setSuccess("Update Profil berhasil");
            response.setData(member);
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }

    public ResponseInfo profileImage(RequestInfo request, MultipartFile file, LoginRq loginRq){
        ResponseInfo response = new ResponseInfo();
        try {
            Membership member = simsRepository.getMember(loginRq.getEmail(), loginRq.getPassword());
            if(ObjectUtils.allNull(member))
                throw new CommonException(HttpStatus.NOT_FOUND, "User tidak ditemukan");

            if(ObjectUtils.allNull(file) || !localStorageService.validateImageFormat(file))
                throw new CommonException(HttpStatus.BAD_REQUEST, "Format Image tidak sesuai"); 

            String separator = FileSystems.getDefault().getSeparator();
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            String mergeName = CommonUtils.strByDate() + "." + extension;
            String localFullPath = localStorageService.getResourcesPath() + separator + mergeName;
            String publicFullPath = CommonUtils.getCurrentEndpoint(request.getHttpServletRequest()) + separator + "resources" + separator + mergeName;

            byte[] bytes = file.getBytes();
            localStorageService.getResourcesPath();
            Files.write(Paths.get(localFullPath), bytes);
            log.info(AppConstant.LOG_DEFAULT_INFO, "saved files", localFullPath);

            member.setProfileImage(publicFullPath);
            simsRepository.updateMembership(member);

            response.setSuccess("Update Profil Image berhasil");
            response.setData(member);
        } catch (Exception e) {
            response.setException(e);
            log.error(AppConstant.LOG_CASE_ERROR, request.getOperationName(), e);
        }
        return response;
    }
}
