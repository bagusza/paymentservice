package com.spring.tht.paymentservice.validator.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.spring.tht.paymentservice.config.properties.AppProperties;
import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.enums.PatternType;
import com.spring.tht.paymentservice.validator.ValidPatternValue;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ValidPatternValueImpl implements ConstraintValidator<ValidPatternValue, String> {
    @Autowired
    @Qualifier(AppConstant.BEAN_APP_CONF)
    private AppProperties appProperties;

    /**
     * pattern type
     */
    private PatternType type;

    @Override
    public void initialize(ValidPatternValue constraintAnnotation) {
        this.type = constraintAnnotation.type();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return StringUtils.defaultString(s, "").matches(appProperties.getPATTERN().get(this.type.name()));
    }
}

