package com.spring.tht.paymentservice.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.spring.tht.paymentservice.model.enums.PatternType;
import com.spring.tht.paymentservice.validator.impl.ValidPatternValueImpl;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidPatternValueImpl.class)
@Documented
public @interface ValidPatternValue {
    String message() default "{ValidPatternValue.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    PatternType type(); // pattern type
}