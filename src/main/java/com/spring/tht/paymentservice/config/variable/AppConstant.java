package com.spring.tht.paymentservice.config.variable;

import java.util.ArrayList;
import java.util.List;

import com.spring.tht.paymentservice.model.exception.ExceptionMap;

public class AppConstant {
    public static final String APP_NAME = "paymentservice";
    public static final String BEAN_APP_CONF= "app-config";
    public static final String BEAN_DS_SIMS_POSTGRES = "ds-sims-postgres";
    public static final String BEAN_JDBC_SIMS_POSTGRES = "jdbc-sims-postgres";
    public static final String CONFIG_DS_SIMS_POSTGRES = "datasource.sims-postgres";

    public static final String LOG_REQUEST_RECEIVED = "[{}][{}][REQUEST RECEIVED][{}]";
    public static final String LOG_REQUEST_END = "[{}][{}][REQUEST END][{}]";
    public static final String LOG_CASE_ERROR = "[ERROR] Error occurred execute {} cause [{}]";
    public static final String LOG_DEFAULT_ERROR = "[ERROR] Error occured {} cause {}";
    public static final String LOG_DEFAULT_INFO = "[INFO] {} {}";

    public static final List<ExceptionMap> EXCEPTION_MAP = new ArrayList<>();

    public enum TRANSACTION_TYPE{
        TOPUP,
        PAYMENT
    }
}