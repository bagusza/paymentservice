package com.spring.tht.paymentservice.config.properties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.spring.tht.paymentservice.model.exception.ExceptionMap;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties("app")
public class AppProperties {
    //APP Config
    private String APP_NAME = "paymentservice";
    private String APP_URL = "localhost:8080";
    private String APP_PARAM = "PAYMENTSRV_";
    private String DOCS_URL = "";
    private int REST_TEMPLATE_TIMEOUT = 10000;
    private boolean LOG_ELK = false;
    private String JWT_SECRET = "";
    private long JWT_EXPIRATION = 0;

    private Map<String,String> PATTERN = new HashMap<>();
    private Map<String,String> SERVICE_MINIO = new HashMap<>();
    private Map<String,String> QUERY_SIMS = new HashMap<>();
    private List<ExceptionMap> EXCEPTION_LIST = new ArrayList<>();


    @PostConstruct
    private void initExceptionList(){
        AppConstant.EXCEPTION_MAP.addAll(EXCEPTION_LIST);
    }
}