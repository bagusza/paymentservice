package com.spring.tht.paymentservice.config.client;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;

import com.spring.tht.paymentservice.config.variable.AppConstant;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class SimsDataSourceConfig {
    @Profile("!local")
    @Bean(name = AppConstant.BEAN_DS_SIMS_POSTGRES)
    @ConfigurationProperties(AppConstant.CONFIG_DS_SIMS_POSTGRES)
    public DataSource dataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean(AppConstant.BEAN_JDBC_SIMS_POSTGRES)
    public JdbcTemplate jdbcTemplate(@Qualifier(AppConstant.BEAN_DS_SIMS_POSTGRES) DataSource datasource) {
        return new JdbcTemplate(datasource);
    }
}
