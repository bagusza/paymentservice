package com.spring.tht.paymentservice.config.variable;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.spring.tht.paymentservice.config.properties.AppProperties;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.tags.Tag;

@Configuration
@OpenAPIDefinition(info = @io.swagger.v3.oas.annotations.info.Info(title = "API Contract SIMS PPOB", version = "v1"))
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, bearerFormat = "JWT", scheme = "bearer")
public class OpenAPIConfig {
        @Autowired
        @Qualifier(AppConstant.BEAN_APP_CONF)
        private AppProperties appProperties;

    @Bean
    public OpenAPI springShopOpenAPI() {

        List<Tag> tags = new LinkedList<>();
        tags.add(new Tag().name("1. Module Membership"));
        tags.add(new Tag().name("2. Module Information"));
        tags.add(new Tag().name("3. Module Transaction"));

        return new OpenAPI()
                .info(new Info()
                        .title(appProperties.getAPP_NAME())
                        .version("prd"))
                .externalDocs(new ExternalDocumentation()
                        .description("Confluence - Service Documentation")
                        .url(appProperties.getDOCS_URL()))
                .tags(tags);
    }
}